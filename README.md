# Lesson 45. Kubernetes 2. YAMLs

General
------------------------
#### List all Resources
```
kubectl get [RESOURCE]
```
#### Describe Resource
```
kubectl describe [RESOURCE]
```
#### Create new Resource
```
kubectl create [RESOURCE] [NAME]
```
#### Delete Resource
```
kubectl delete [RESOURCE] [NAME]
```

Namespace
------------------------
#### [List/Describe/Create/Delete [RESOURCE=ns|namespace]](#general)


Pods
------------------------
#### [List/Describe/Delete [RESOURCE=pod]](#general)
#### Run pod
```
kubectl run [NAME] --image=[IMAGE] --namespace=[NAMESPACE] --port=[PORT]
```
#### Forward ports for container
```
kubectl port-forward [NAME] [HOST_PORT]:[INTERNAL_PORT] --namespace=[NAMESPACE]
```


Deployments
------------------------
#### [List/Describe/Create/Delete  [RESOURCE=deploy|deployment]](#general)


ReplicaSet
------------------------
#### [List/Describe/Delete  [RESOURCE=rs]](#general)
#### Scale Resource
```
kubectl scale [RESOURCE]/[NAME] --replicas=[COUNT] --namespace=[NAMESPACE]
```


HPA (HorizontalPodAutoscalers)
------------------------
#### [List/Describe/Delete  [RESOURCE=hpa]](#general)
#### Create Autoscale
```
kubectl autoscale [RESOURCE]/[NAME] --min=[MIN_COUNT] --max=[MAX_COUNT] --cpu-percent=[VALUE] --namespace=[NAMESPACE]
```


Rollout
------------------------
Information of changes
#### Get History
```
kubectl rollout history [RESOURCE]/[NAME] --namespace=[NAMESPACE]
```
#### Get Status
```
kubectl rollout status [RESOURCE]/[NAME] --namespace=[NAMESPACE]
```
#### Undo to previous revision
```
kubectl rollout undo [RESOURCE]/[NAME] --namespace=[NAMESPACE]
```
#### Undo to specific revision
```
kubectl rollout undo [RESOURCE]/[NAME] --to-revision=[REVISION] --namespace=[NAMESPACE]
```


Other
------------------------
#### Apply file
```
kubectl apply -f=[FILENAME]
```
#### Destroy file
```
kubectl delete -f=[FILENAME]
```
#### Set another image to resource
```
kubectl set image [RESOURCE]/[NAME] [CONTAINER_NAME]=[IMAGE_NATE] --namespace=[NAMESPACE]
```